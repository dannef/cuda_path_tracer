#ifndef CAMERAH
#define CAMERAH

class camera {
public:

  __device__ camera () {}
  __device__ camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect) {
    float theta = vfov * M_PI/180.0f;
    float half_height = tan(theta/2.0f);    
    float half_width = aspect * half_height;

    origin = lookfrom;
    vec3 w = normalize(lookfrom - lookat);
    vec3 u = normalize(cross(vup, w));
    vec3 v = cross(w, u);

    lower_left_corner = vec3(-half_width, -half_height, -1.0);
    lower_left_corner = origin - half_width*u - half_height*v - w;

    horizontal = 2 * half_width*u;
    vertical = 2 *half_height * v;
  }
  __device__ ray get_ray(float s, float t) {
    return ray(origin, lower_left_corner + s*horizontal + t*vertical - origin);
  }
  
  vec3 lower_left_corner;
  vec3 horizontal;
  vec3 vertical;
  vec3 origin;

};
#endif

#ifndef POINTLIGHTH
#define POINTLIGHTH

class point_light {
public:

  __device__ point_light () {}
  __device__ point_light(vec3 p, vec3 c) : position(p), color(c){}
  
  vec3 position;
  vec3 color;
};


#endif

#ifndef RAYH
#define RAYH

class ray {
public:
  __device__ ray() {}
  __device__ ray(vec3 o, vec3 dir) :  origin(o), direction(dir) {}
  __device__ vec3 ray_current_position(float t) const {
    return origin + t*direction;
  }
  vec3 origin;
  vec3 direction;
  
};

#endif

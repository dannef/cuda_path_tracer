#ifndef VEC3H
#define VEC3H


#include <math.h>
// create class and use inline functions for pretty much every call


class vec3 {
 public :

  __host__ __device__ vec3() {}
  __host__ __device__ vec3(float x) : x(x), y(x), z(x){}
  __host__ __device__ vec3(float x, float y, float z) :  x(x), y(y), z(z) {}

  __host__ __device__ inline vec3 operator-() const { return vec3(-x, -y, -z);}
  float x;
  float y;
  float z;
  
};
__host__ __device__ inline vec3 cross(const vec3 &v1, const vec3 &v2) {
    return vec3( (v1.y*v2.z - v1.z*v2.y),
                (-(v1.x*v2.z - v1.z*v2.x)),
                (v1.x*v2.y - v1.y*v2.x));
}

__host__ __device__ inline float dot(const vec3 &a, const vec3 &b){
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

__host__ __device__ inline vec3 operator+(const vec3 &a, const vec3 &b){
  return vec3(a.x+b.x, a.y+b.y, a.z+b.z);
}

__host__ __device__ inline vec3 operator-(const vec3 &a, const vec3 &b){
  return vec3(a.x-b.x, a.y-b.y, a.z-b.z);
}

__host__ __device__ inline vec3 operator*(float a, const vec3 &b){
  return vec3(a*b.x, a*b.y, a*b.z);
}

__host__ __device__ inline vec3 operator*(const vec3 &a, const vec3 &b){
  return vec3(a.x*b.x, a.y*b.y, a.z*b.z);
}

__host__ __device__ inline vec3 operator*(const vec3 &b, float a){
  return vec3(a*b.x, a*b.y, a*b.z);
}

__host__ __device__ inline vec3 operator/(const vec3 &a, const vec3 &b){
  return vec3(a.x/b.x, a.y/b.y, a.z/b.z);  
}

// Other good functions
__host__ __device__ inline float length(const vec3 &v){
  return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

__host__ __device__ inline vec3 normalize(const vec3 &v){
  return v / length(v);
}

#endif

#ifndef SAMPLINGH
#define SAMPLINGH

#define M_PI 3.14159265359


__device__ float fabs_(float val){
  if(val > 0)
    return val;
  else
    return -val;
}


__device__ void create_coordinate_system(vec3 &N, vec3 &Nt, vec3 &Nb){

  if(fabs_(N.x) > fabs_(N.y))
    Nt = vec3(N.z, 0, -N.x) / sqrtf(N.x * N.x + N.z*N.z);
  else
    Nt = vec3(0, -N.z, N.y) / sqrtf(N.y * N.y + N.z*N.z);
  
  Nb = cross(N, Nt);
  
}

__device__ vec3 uniform_sample_hemisphere(curandState *local_rand_state){
  float r1 = curand_uniform(local_rand_state);
  float r2 = curand_uniform(local_rand_state);
  float sin_theta = sqrtf(1 - r1*r1);
  float phi = 2.0f*M_PI*r2;
  // Spherical coordinates:
  float x = sin_theta*cosf(phi);
  float z = sin_theta*sinf(phi);

  // Cartesian -> Spherical yields, with right hand coordinate system:
  // x = sintheta cos phi
  // y = cos theta
  // z = sin theta sin phi
  return vec3(x, r1, z);
}

__device__ vec3 cosine_weighted_sample(curandState *local_rand_state){
  float r1 = curand_uniform(local_rand_state);
  float r2 = curand_uniform(local_rand_state);

  float c1 = sqrtf(r1);
  float c2 = 2.0f*M_PI*r2;
  
  
  float x = cosf(c2)*c1;  
  float y = sqrtf(1 - r1);
  float z = sinf(c2)*c1;

  return vec3(x,y,z);
}
__device__ vec3 cosine_sample_lapere(curandState *local_rand_state){
  vec3 w = vec3(0,1,0);
  vec3 u = normalize(cross((fabs(w.x) > .1f ? vec3(0, 1, 0) : vec3(1, 0, 0)), w));  
  vec3 v = cross(w,u);
  
  float r1 = 2* M_PI * curand_uniform(local_rand_state);
  float r2 = curand_uniform(local_rand_state);
  float r2s = sqrt(r2);
  
  return normalize(u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrtf(1 - r2));
}
#endif

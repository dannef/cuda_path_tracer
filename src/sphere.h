#ifndef SPHEREH
#define SPHEREH



class sphere {
public:
  __host__ __device__ sphere() {}
  __host__ __device__ sphere(vec3 c, float r, vec3 col, vec3 e, material *m, int type) : center(c), radius(r), color(col),
											 emittance(e), mat_ptr(m), material_type(type) {}
    
  vec3 emittance;
  vec3 color;
  vec3 center;
  float radius;
  material *mat_ptr;
  int material_type;
};


#endif

#ifndef MATERIALH
#define MATERIALH




__device__ float schlick(float cosine, float ref_idx){
  float r0 = (1.0f-ref_idx) / (1.0f+ref_idx);
  r0 = r0*r0;
  float x = 1.0f - cosine;
  //TODO: remove use of pow function, its slow, just t
  return r0 + (1.0f-r0)*x*x*x*x*x; // pow((1.0f-cosine),5.0f);
}    

__device__ vec3 reflect(const vec3 &v, const vec3 &n){
  return v - 2.0f*dot(v,n)*n;
}

__device__ bool refract(const vec3 &v, const vec3 &n, float ni_over_nt, vec3 &refracted){
  vec3 uv = normalize(v);
  float dt = dot(uv, n);

  float disc = 1.0f - ni_over_nt*ni_over_nt*(1.0f-dt*dt);
  if(disc > 0.0f){
      refracted = ni_over_nt * (uv - n*dt) - n * sqrt(disc);
      return true;
    }

  return false;
}

class material {

public:

  __device__ virtual vec3 sample_direction(vec3 wo, vec3 normal, curandState *local_rand_state) = 0;

};

class diffuse : public material {

public:
  __device__ diffuse() {}
  __device__ vec3 sample_direction(vec3 wo, vec3 N, curandState *local_rand_state){
    vec3 Nt, Nb;
    create_coordinate_system(N, Nt, Nb);
      
    vec3 sample = cosine_weighted_sample(local_rand_state);
    sample = vec3(sample.x * Nb.x + sample.y * N.x + sample.z * Nt.x, 
		  sample.x * Nb.y + sample.y * N.y + sample.z * Nt.y, 
		  sample.x * Nb.z + sample.y * N.z + sample.z * Nt.z);

    return normalize(sample);
  }


};
class specular : public material{
public:
  __device__ specular() {}
  __device__ vec3 sample_direction(vec3 wo, vec3 normal, curandState *local_rand_state){
    vec3 wi = -wo + 2.0f * (dot(wo, normal)) * normal;
    return wi;    
  }
};

class fresnel_refract : public material {
public:
  __device__ fresnel_refract() {}
  __device__ vec3 sample_direction(vec3 wo, vec3 normal, curandState *local_rand_state){
    return vec3(0);

    
  }

  float eta; // Air is assumed otherwise.
};
#endif

#ifndef HITRECORDH
#define HITRECORDH

struct hit_record {
  vec3 normal;
  vec3 position;
  float t;
}

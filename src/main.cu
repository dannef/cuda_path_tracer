#include "math_help.h"
#include "ray.h"
#include <curand_kernel.h>
#include "sampling.h"
#include "material.h"
#include "sphere.h"
#include <iostream>
#include <time.h>
#include "point_light.h"
#include "camera.h"



/* Here are some notes of the random dumb compiler messages the C++ or CUDA?? returns when there
is a compiler error and the error is confusing and lackluster.

Expression must have class type - usually means we are using . instead of -> (note pointer).

*/
#define SAMPLING 0
#define RENDER 1
#define checkCudaErrors(val) check_cuda((val), #val, __FILE__, __LINE__)

enum material_types { DIFFUSE, SPECULAR, FRESNEL_REFRACT, SPECULAR_DIFFUSE };
void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line){
  if(result) {
    std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
      file << ":" << line  << " '" << func << "' \n";

    // reset CUDA device before exiting;

    cudaDeviceReset();
    exit(99);
  }
}

struct hit_record {
  vec3 p;
  vec3 normal;
  float t;
};


__device__ bool hit_sphere(const vec3& center, float radius, float t_min, float t_max, const ray& r, hit_record &rec) {
  
  vec3 oc = r.origin - center;
  float a = dot(r.direction, r.direction);
  float b = dot(oc, r.direction);
  float c = dot(oc, oc) - radius*radius;

  float disc = b*b - a*c;

  if(disc > 0 ){
    float temp = (-b - sqrt(disc)) / a;
    if(temp < t_max && temp > t_min){
      rec.t = temp;
      rec.p = r.ray_current_position(temp);      
      rec.normal = (rec.p - center) / radius;
      return true;
    }
    temp = (-b + sqrt(disc))/a;
    if(temp< t_max && temp > t_min){
      rec.t = temp;
      rec.p = r.ray_current_position(temp);
      rec.normal = (rec.p -center) / radius;
      return true;
    }
  }
  return false;
}

__global__ void create_world(sphere **d_list, point_light **d_light_list, camera **cam){
  if(threadIdx.x == 0 && blockIdx.x == 0){

    d_list[0] = new sphere(vec3(-0.7,0,-0.6), 0.5f, vec3(0.0, 0.5, 0.96), vec3(0), new diffuse(), DIFFUSE);
    d_list[1] = new sphere(vec3(0.8,0,-0.6), 0.5f, vec3(1.0, 1.0, 1.0), vec3(0), new diffuse(), SPECULAR_DIFFUSE);
    // d_list[2] = new sphere(vec3(0.8,0.3,-0.6), -0.49f, vec3(0.0, 0.5, 0.96), vec3(0), new specular(), FRESNEL_REFRACT);
    d_list[2] = new sphere(vec3(0,-100.5f,-1), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //bottom
    d_list[3] = new sphere(vec3(-102.0f, 0, -1), 100.0f, vec3(0.75f, 0.25f, 0.25f), vec3(0), new diffuse(), DIFFUSE); // left
    d_list[4] = new sphere(vec3(102.0f, 0.0f,-1), 100.0f, vec3(0.25f, 0.25f, 0.75f), vec3(0), new diffuse(), DIFFUSE); //right
    d_list[5] = new sphere(vec3(0.0f, 0.0f,-102.0f), 100.0f, vec3(0.75f, 0.75f, 0.75f), vec3(0), new diffuse(), DIFFUSE); //back
    d_list[6] = new sphere(vec3(0.0f, 102.0f,-1.0), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //top
    d_list[7] = new sphere(vec3(0.0f, 0.0f,105.0f), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //front    
    d_list[8] = new sphere(vec3(0.0f, 31.995f, -.8), 30.0f, vec3(0), vec3(3.5), new diffuse(), DIFFUSE);

    d_light_list[0] = new point_light(vec3(1.1f, 1.0f,-1), vec3(1.0, 1.0, 1.0));
    d_light_list[1] = new point_light(vec3(-1.1, 1.0, -1), vec3(1.0, 1.0, 1.0));


    cam[0] = new camera(vec3(0.0,0.5,1.0f), vec3(0,0,-1), vec3(0,1,0), 90, 1200.0f/600.0f);
    /*
    cam[0] = new camera(vec3(-2.0, -1.0f, -1.0f),
			vec3(4.0f, 0.0f, 0.0f),
			vec3(0.0f, 2.0f, 0.0f),
			vec3(0.0f, 0.0f, 0.0f));
    */
  }
}
// TODO: Have to test the emitted function see if it will actually do contribution.
// Create emitting spheres.
__device__ inline bool intersect_scene(sphere **spheres, int nr_of_spheres, const ray &r, hit_record &hit_rec, int &id){
  float closest_t = FLT_MAX;  
  bool intersected = false;
  for(int k = 0; k < nr_of_spheres; k++){
    hit_record rec;
    if(hit_sphere(spheres[k]->center, spheres[k]->radius, 0.001f, closest_t, r, rec)){
      intersected = true;
      if(rec.t < closest_t){	  
	closest_t = rec.t;
	id = k;
	hit_rec = rec;
      }
    }
  }
  if(intersected)
    return true;
  
  return false; 
}

__device__ vec3 indirect_illumination(sphere **spheres, int nr_of_spheres, ray &r, curandState *local_rand_state){

  vec3 L = vec3(0.0);
  vec3 path_throughput = vec3(1.0);

  hit_record rec;
  // if(hit_sphere(spheres[8]->center, spheres[8]->radius, 0.001f, FLT_MAX, r, rec))
  //return vec3(1);
  //TODO: Currently the emitting spheres are rendered with higher emitting values, which causes weird aliasing when we are rendering them
  //I am assuming that this is due to the fact that they contribute more than what we allow, i.e., values greater than 1.0, which when we
  //divide later, the color of the sphere is not correct.
  for(int bounces = 0; bounces < 8; bounces++){
    
    hit_record temp_rec;
    int id = 0;
    if(!intersect_scene(spheres, nr_of_spheres, r, temp_rec, id))
      return vec3(0.0);


    vec3 n = temp_rec.normal;


    // Add emission from object.
    
    L = L + path_throughput * spheres[id]->emittance;

    vec3 sampled_direction;
    if(spheres[id]->material_type == DIFFUSE){
      vec3 nl = n ; //dot(n, r.direction) < 0.0f ? n*-1.0f : n ;  //TODO: Check why they use this in smallpt?
      sampled_direction = spheres[id]->mat_ptr->sample_direction(vec3(1.0),nl, local_rand_state);
      r.origin = temp_rec.p + 0.001f * nl;
      r.direction = sampled_direction;//normalize(sample);
      path_throughput = path_throughput * spheres[id]->color * dot(r.direction, nl) * 2.0f;
    }
    if(spheres[id]->material_type == SPECULAR){
      vec3 nl = n ; // dot(n, -r.direction) < 0.0f ? n*-1.0f : n;
      sampled_direction = spheres[id]->mat_ptr->sample_direction(normalize(-r.direction), nl, local_rand_state);
      r.origin = temp_rec.p + 0.001f*nl;
      r.direction = normalize(sampled_direction);
    }
    if(spheres[id]->material_type == SPECULAR_DIFFUSE){
      float rand = curand_uniform(local_rand_state);
      float cosine = dot(r.direction, n);

      if ( cosine < 0.0f) cosine = -cosine;
      float glass = 1.5f;
      float reflect_prob = schlick(cosine, glass);

      float P = (reflect_prob + 0.5f) / 2.0f;
      
      
      if(rand <= P){
	vec3 wi = r.direction + 2.0f * (dot(-r.direction, n)) * n;
	r.origin = temp_rec.p + 0.001f*n;
	r.direction = normalize(wi);
	path_throughput = (path_throughput * reflect_prob) / P;
      }
      else{
	sampled_direction = spheres[id]->mat_ptr->sample_direction(vec3(1.0),n, local_rand_state);
	r.origin = temp_rec.p + 0.001f * n;
	r.direction = sampled_direction;//normalize(sample);	
	path_throughput = path_throughput * spheres[id]->color * dot(r.direction, n) * 2.0f * (1.0f-reflect_prob)  / (1.0f - P);// * (1.0f - reflect_prob) / (1.0f - P);
      }
            
    }    
    if(spheres[id]->material_type == FRESNEL_REFRACT){

      vec3 refracted;

      float cosine = dot(n, -r.direction);
      float glass = 1.52f;
      float eta = 1.0f / glass;
      
      float reflect_prob;      
      if(dot(-r.direction, n) < 0.0f){ // inside	
	n = -n;
	eta = glass;
	cosine = -cosine;

      }

      if(refract(r.direction, n, eta, refracted))
	reflect_prob = schlick(cosine, glass);
      else
	reflect_prob = 1.0f;
      
      float rand = curand_uniform(local_rand_state);
      float P = (reflect_prob + 0.5f) / 2.0f;
      float k = 0.9f;
      float p = (k / 2.0f) + (1.0f - k)*reflect_prob;
      if(rand <= p){
	r.origin = temp_rec.p + 0.001f*n;
	vec3 wi = r.direction + 2.0f * (dot(-r.direction, n)) * n;
	r.direction = normalize(wi);
	//path_throughput = (path_throughput * reflect_prob) / p;
      }
      else{
	r.origin = temp_rec.p + 0.001f*-n;
	r.direction = normalize(refracted);
	path_throughput = path_throughput * (1.0f - reflect_prob)/(1.0f - p);
	
      }
      
    }      
      
  }
  return L;

}
__global__ void render_init(int max_x, int max_y, curandState *rand_state) {

  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;

  if(i >= max_x || j >= max_y) return;

  int pixel_index = j*max_x + i;
  curand_init(1984, pixel_index, 0 , &rand_state[pixel_index]);
}
__global__ void render_init2(int max_x, curandState *rand_state) {

  int i = threadIdx.x ;


  if(i >= max_x) return;
  
  curand_init(1984, i, 0 , &rand_state[i]);
}

__global__ void gen_samples(vec3 *fb2 , curandState *rand_state){
  
  int i = threadIdx.x;

  if(i >= 256) return;
  curandState local_rand_state = rand_state[i];

  vec3 Nt;
  vec3 Nb;
  vec3 N = vec3(1, 0, 0);
  create_coordinate_system(N, Nt, Nb);
  //vec3 sample = uniform_sample_hemisphere(&local_rand_state);
  vec3 sample = cosine_weighted_sample(&local_rand_state);
  vec3 sample_world_space(sample.x * Nb.x + sample.y * N.x + sample.z * Nt.x, 
			  sample.x * Nb.y + sample.y * N.y + sample.z * Nt.y, 
			  sample.x * Nb.z + sample.y * N.z + sample.z * Nt.z);
  fb2[i] = sample_world_space;
}
// Shoot a shadow ray in a given direction and test against spheres, return on hit.
__device__ bool test_shadow_ray(sphere **spheres, int nr_of_spheres, ray &r, float light_dis){
  for(int k = 0; k < nr_of_spheres; k++){
    hit_record rec;
    if(hit_sphere(spheres[k]->center, spheres[k]->radius, 0.01f, light_dis, r, rec))
      return true;
  }  
  return false;
}
__device__ vec3 direct_illumination(sphere **spheres, int nr_of_spheres, hit_record rec, point_light **lights, int nr_of_lights, int object_hit_id){
  vec3 col = vec3(0,0,0);
  for(int i = 0; i < nr_of_lights; i++){
    ray shadow_ray;
    shadow_ray.origin = rec.p + 0.01f*rec.normal;
    shadow_ray.direction = normalize(lights[i]->position - shadow_ray.origin);
    float d = length(lights[i]->position - shadow_ray.origin);
    
    if (d < 1.0f)    // Avoid division by zero, point light really only work when they are atleast a unit away
      d = 1.0f;
    if(!test_shadow_ray(spheres, nr_of_spheres, shadow_ray, d)){    
       float cosine_attenuation = dot(rec.normal, normalize(shadow_ray.direction));
       col = col + spheres[object_hit_id]->color * lights[i]->color * (1.0f / (d*d)) * max(0.0f, cosine_attenuation);
    }
  }
  return col;
}
	
  

  // For all lights
  
// NOTE: DO NOT SEND REFERENCES OF DATA INTO THE __global__ FUNCTIONS AS I AM GUESSING
// THIS WILL CAUSE RACE CONDITIONS, I.E., UNWANTED ONES FOR NOW,
// AGAIN COMPILER DOES NOT WARN US ABOUT THIS.
__global__ void render(vec3 *fb, int max_x, int max_y, int ns, sphere **spheres, int nr_of_spheres,
		       curandState *rand_state,
		       camera **cam,
		       point_light **lights,
		       int nr_of_lights){
  
  int i = threadIdx.x + blockIdx.x *blockDim.x;
  int j = threadIdx.y + blockIdx.y *blockDim.y;

  if(i >= max_x || j >= max_y) return;

  int pixel_index = j * max_x + i;
  curandState local_rand_state = rand_state[pixel_index];
  
  vec3 col = vec3(0,0,0);
  // NOTE: DO NOT USE EXISTING FUCKING VARIABLES IN THE FOR CONDITIONAL BECAUSE THE
  // CUDA COMPILER DOES NOT SEEM TO DIFFERENTIATE BETWEEN LOCAL VARIABLES. HOWEVER
  // THIS MIGHT BE A SPECIAL CASE FOR __global__ FUNCTIONS? DOESNT EVEN WARN YOU THOUGH
  for(int s = 0; s < ns; s++){
    float u = float(i + curand_uniform(&local_rand_state)) / float(max_x);
    float v = float(j + curand_uniform(&local_rand_state)) / float(max_y);

    ray r = (*cam)->get_ray(u,v);
    //CLEANUP: Change the input data in these functions, some of it is not needed anymore and is excessive.
    col = col + indirect_illumination(spheres, nr_of_spheres, r, &local_rand_state);
 
  }
  rand_state[pixel_index] = local_rand_state;
  col = col / float(ns);
  fb[pixel_index] = col;
}

inline float clamp(float x){ return x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x; } 
inline int toInt(float x){ return int(pow(clamp(x), 1 / 2.2) * 255 + .5); }  // convert RGB float in range [0,1] to int in range [0, 255] and perform gamma correction

int main(){

  // Define our camera.
  int nx = 1200;
  int ny = 600;
  int ns = 40000;

  int tx = 8;
  int ty = 8;

  // define 8 by 8 blocks of threads
  // TODO: Check what is actual optimal for block sizes? What are good practices?
  dim3 blocks(nx/ty +1, ny/ty+1);
  dim3 threads(tx,ty);

  vec3 *fb;

  int num_pixels = nx*ny;
  size_t fb_size = num_pixels*sizeof(vec3);


  // Cl
  checkCudaErrors(cudaMallocManaged((void**)&fb, fb_size));

  camera **d_cam;
  checkCudaErrors(cudaMalloc((void**)&d_cam, sizeof(camera)));
  
  sphere **d_spheres;
  int sphericals = 9;
  // Just give me space for 100 spheres.
  checkCudaErrors(cudaMalloc((void **)&d_spheres, 100 * sizeof(sphere)));

  point_light **d_lights;
  int nr_of_lights = 2;
  checkCudaErrors(cudaMalloc((void **)&d_lights, nr_of_lights * sizeof(point_light)));
  
  create_world<<<1,1>>>(d_spheres, d_lights, d_cam);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());

  
  //Allocate memory for the random number sequence for each thread.
  curandState *d_rand_state;
  checkCudaErrors(cudaMalloc((void**)&d_rand_state, num_pixels*sizeof(curandState)));
  checkCudaErrors(cudaGetLastError());


  // Initalize rand_state for every thread
  render_init<<<blocks,threads>>>(nx, ny, d_rand_state);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());
    // --------------------------------- SAMPLING TEST -------------------------------
#if SAMPLING 
  vec3 *fb2;

  size_t fb_size2 = 256*sizeof(vec3);

  checkCudaErrors(cudaMallocManaged((void**)&fb2, fb_size2));

  curandState *d_rand_state2;
  checkCudaErrors(cudaMalloc((void**)&d_rand_state2, 256*sizeof(curandState)));
  checkCudaErrors(cudaGetLastError());

  // Initalize rand_state for every thread
  render_init2<<<1, 256>>>(nx, d_rand_state2);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());

  
  gen_samples<<<1, 256>>>(fb2, d_rand_state2);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());

  //Testing the sampling.
  for (int z = 0; z < 256; z++)
    std::cout << fb2[z].x << ", " << fb2[z].y << ", " << fb2[z].z<< "\n";
#endif
  
// --------------------------------- SAMPLING TEST -------------------------------
#if RENDER  
  std::cerr << "Rendering a " << nx << "x" << ny << " image " << "with " << ns << " samples per pixel ";
  std::cerr << "in " << tx << "x" << ty << " blocks.\n";

    // CPU Timer
  clock_t start, stop;
  start = clock();

  render<<<blocks, threads>>>(fb, nx, ny, ns, d_spheres, sphericals, d_rand_state, d_cam, d_lights, nr_of_lights);

  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());

  stop = clock();
  double timer_seconds = ((double)(stop - start)) / CLOCKS_PER_SEC;
  std::cerr << "took " << timer_seconds << " seconds.\n";

  
  std::cout << "P3\n" << nx << " " << ny << "\n255\n";
  for(int j = ny-1; j >= 0; j--){
    for(int i = 0; i < nx; i++){
      size_t pixel_index = j*nx + i;
      float r = fb[pixel_index].x;
      float g = fb[pixel_index].y;
      float b = fb[pixel_index].z;

      int ir = toInt(r);//(int(255.99*r);
      int ig = toInt(g);//int(255.99*g);
      int ib = toInt(b);//int(255.99*b);
      std::cout << ir << " " << ig << " " << ib << "\n";
    }
  }
  checkCudaErrors(cudaDeviceSynchronize());
  checkCudaErrors(cudaFree(fb));
  checkCudaErrors(cudaFree(d_spheres));
  checkCudaErrors(cudaFree(d_rand_state));
  checkCudaErrors(cudaFree(d_lights));	       
  //  checkCudaErrors(cudaFree(fb2));
  //checkCudaErrors(cudaFree(d_rand_state2));
#endif  
  cudaDeviceReset();
  
  return 0;
  
}

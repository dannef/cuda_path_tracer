## Path tracer in CUDA 

Basic brute-force path tracer. 
----

![alt text](renders/out7.png/ "first ok render, cosine sampled diffuse")
**1200x600 @ 200k spp**

----


![alt text](renders/out9.png/ "second render, diffuse, specular mats")
**1200x600 @ 100k spp @ 8 bounces per path, diffuse and specular material**

----
![alt text](renders/out10.png/ "first transmittance brdf render")
**1200x600 @ 100k spp @ 8 bounces per path, glass material**

----
![alt text](renders/out12.png/ "Need to sample lights...")
**1200x600 @ 100k spp @ 8 bounces per path**

----
![alt text](renders/out14.png/ "specular diffuse material")
**1200x600 @ 100k spp @ 8 bounces per path, specular-diffuse material**
